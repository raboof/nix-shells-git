# nix-shell-git

Keep `shell.nix` files aside from your source repo's.

## Why?

The [Nix](https://nixos.org/) package manager allows you to keep per-project
dependency/environment specifications in `shell.nix` files.

With `nix-shell-git` you can load your `shell.nix` files from a separate
`~/nix-shells` tree, making it easy version and share them.

## Usage

Create a `~/nix-shells` directory to hold your `shell.nix` files. The format
follows your git repo host- and path names. For an example, see
[my nix-shells](https://codeberg.org/raboof/nix-shells).

Then, just `cd` into a project directory and run `nix-shell-git` to start a
`nix-shell` with the `shell.nix` for that project.

## TODO

[here](https://codeberg.org/raboof/nix-shell-git/issues)

## Similar projects

There are also other ways to keep some Nix configurations aside the project
repo:

* [git-along](https://github.com/nyarly/git-along) integrates with git more tightly. TBQH I don't quite understand where the files actually go :).
* [shellbit](https://github.com/ivanbrennan/shellbit#shellbit) puts them all under a top-level nix expression, so you can load them right into nix-shell with `-A`. An example repo is at [nixels](https://github.com/ivanbrennan/nixels).
* [nix-environments](https://github.com/nix-community/nix-environments) also just has a top-level nix expression without any particular tooling.

Know more? Please share ;)
